// ==UserScript==
// @name         鉴别单处理
// @namespace    http://tampermonkey.net/
// @version      0.1.2
// @description  上传鉴别单信息
// @author       pelebl
// @match        https://paimai.taobao.com/console/seller/*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=taobao.com
// @grant        none
// @require      https://cdn.bootcdn.net/ajax/libs/jquery/3.5.1/jquery.js
// @updateURL    https://gitlab.com/older-open/userscripts/-/raw/main/paimain-jianding.user.js
// @downloadURL  https://gitlab.com/older-open/userscripts/-/raw/main/paimain-jianding.user.js
// ==/UserScript==

var stopRun = false;

function sleep(time) {
  return new Promise((resolve) => setTimeout(resolve, time));
}

function syncSleep(milliseconds) {
  const date = Date.now();
  let currentDate = null;
  do {
    currentDate = Date.now();
  } while (currentDate - date < milliseconds);
}

function getOrderNo(tr, trCode) {
  console.log('getOrderNo');
  var tmp = tr.find('span.basic-model-text');
  var orderNo = $(tmp[0]).text();
  var goodName = $(tmp[1]).text();
  var qty = $(tmp[2]).text();
  var status = $(tmp[3]).text();
  var organ_name = $(tmp[4]).text();
  var orderTime = $(tmp[6]).text();
  if (goodName == '退款关闭') {
    goodName = $(tmp[2]).text();
    qty = $(tmp[3]).text();
    status = $(tmp[4]).text();
    organ_name = $(tmp[5]).text();
    orderTime = $(tmp[6]).text();
  }

  if (status == '待机构收货') {
    console.log(orderNo);
    $('#peleBtn').text(`[${orderTime}] ${orderNo}`);
    tmp = $(trCode).find('div.next-table-body:first').find('.next-table-row');
    var jiandingCode = [];
    for (const code of tmp) {
      var text = $(code).find('span.basic-model-text');
      jiandingCode.push($(text[1]).text());
      // $(text[1]).text()
      // console.log();
    }

    console.log(jiandingCode.join(','));

    // $.post('https://api.older.cn/api/v1/appraisal/put_new', {
    //   platform_code: orderNo,
    //   platform_item_name: goodName,
    //   qty: parseInt(qty.replace('件', '')),
    //   organ_name: organ_name,
    //   identification_number: jiandingCode.join(',')
    // });

    $.ajax({
      contentType: 'application/json',
      data: JSON.stringify({
        platform_code: orderNo,
        platform_item_name: goodName,
        qty: parseInt(qty.replace('件', '')),
        organ_name: organ_name,
        identification_number: jiandingCode.join(','),
      }),
      dataType: 'json',
      success: function (data) {
        console.log('done');
      },
      error: function () {
        console.log('failed');
      },
      processData: false,
      type: 'POST',
      url: 'https://api.older.cn/api/v1/appraisal/put_new',
    });
  }

  if (status == '已发给买家') {
    console.log(orderNo);
    let note = '';
    let expressCompany = '';
    let expressNumber = '';
    if ($(tmp[4]).text() === '中国收藏家协会（杭州）') {
      expressCompany = $(tmp[12]).text();
      expressNumber = $(tmp[13]).text();
    } else {
      note = $(tmp[4]).text();
      expressCompany = $(tmp[13]).text();
      expressNumber = $(tmp[14]).text();
    }

    $.ajax({
      contentType: 'application/json',
      data: JSON.stringify({
        platform_code: orderNo,
        express_company: expressCompany,
        express_number: expressNumber,
        note: note,
      }),
      dataType: 'json',
      success: function (data) {
        console.log('done');
        console.log(data);
      },
      error: function (err) {
        console.log('failed');
      },
      processData: false,
      type: 'POST',
      url: 'https://api.older.cn/api/v1/appraisal/put_express',
    });
  }
}

function doPage() {
  var i = 0;
  let jiance = setInterval(() => {
    i++;
    console.log('第' + i + '次检测');
    if (i > 19) {
      //清除延迟
      clearInterval(jiance);
    }

    console.log($('tr.next-table-row').length);
    if ($('tr.next-table-row').length > 0) {
      clearInterval(jiance);

      console.log('未展开数量' + $('.next-table-expand-fold').length);
      for (const fold of $('.next-table-expand-fold')) {
        $(fold).click();
      }
      var trOrder, trCode;
      trOrder = $('tr.next-table-row.first:first');
      while (trOrder.length > 0) {
        if (stopRun) {
          return;
        }

        trCode = $(trOrder).next();
        getOrderNo(trOrder, trCode);
        syncSleep(100);
        trOrder = $(trCode).next();
      }

      // console.log($('.next-pagination-list > button.next-pagination-item:first').attr('aria-label'));

      console.log($('button.next-next').length);
      if ($('button.next-next').attr('disabled') == undefined) {
        $('button.next-next').click();
        sleep(1000).then(() => {
          doPage();
        });
      } else {
        if (
          $('.next-pagination-list > button.next-pagination-item:first')
            .length > 0 &&
          $('.next-pagination-list > button.next-pagination-item:first').is(
            '.next-current'
          ) == false
        ) {
          $(
            '.next-pagination-list > button.next-pagination-item:first'
          ).click();
          sleep(1000).then(() => {
            doPage();
          });
        }
      }
    }
  }, 500);
}

(function () {
  'use strict';

  // Your code here...
  $(() => {
    console.log('我来啦');
    //一个是新元素，一个是body元素
    var mybutton, beasetag, printDiv, barcode;
    //创建新元素
    mybutton = document.createElement('div');
    //搜寻body元素
    beasetag = document.querySelector('body');
    //将新元素作为子节点插入到body元素的最后一个子节点之后
    beasetag.appendChild(mybutton);
    //可以通过mybutton.innerHTML = "<button type='button'>启动</button><br><button type='button'>关闭</button>"来写入其他元素，如多个按钮
    mybutton.innerHTML = '点击开始扫描';
    //css样式为
    //position:fixed;生成固定定位的元素，相对于浏览器窗口进行定位。元素的位置通过 "left", "top", "right" 以及 "bottom" 属性进行规定。
    //bottom:15px;距窗口底部15px
    //right:15px;距窗口右边15px
    //width:60px;内容的宽度60px
    //height:60px;内容的高度60px
    //background:black;内边距的颜色和内容的颜色设置为黑色，不包括外边距和边框
    //opacity:0.75;不透明度设置为0.75，1为完全不透明
    //color:white;指定文本的颜色为白色
    //text-align:center;指定元素文本的水平对齐方式为居中对齐
    //line-height:60px;设置行高，通过设置为等于该元素的内容高度的值，配合text-align:center;可以使div的文字居中
    //cursor:pointer;定义了鼠标指针放在一个元素边界范围内时所用的光标形状为一只手
    mybutton.style =
      'position:fixed;bottom:20%;right:30%;min-width:300px;width:auto;height:60px;background:black;opacity:0.75;color:white;text-align:center;line-height:60px;cursor:pointer;';
    mybutton.id = 'peleBtn';

    $('#peleBtn').click(function () {
      if ($('#peleBtn').text() == '点击开始扫描') {
        stopRun = false;
        doPage();
      } else {
        stopRun = true;
        $('#peleBtn').text('点击开始扫描');
      }
    });
    // doPage();
  });
})();
