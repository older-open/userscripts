// ==UserScript==
// @name         拍卖的商品管理
// @namespace    http://tampermonkey.net/
// @version      0.0.1
// @description  为每个商品添加一个按钮，点击后可以查看商品的详情
// @author       pele
// @match        https://paimai.taobao.com/console/seller/index.htm?*
// @match        https://sku.m.taobao.com/index?*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=taobao.com
// @require      https://cdn.bootcdn.net/ajax/libs/jquery/3.5.1/jquery.js
// @grant        GM_setValue
// @grant        GM_getValue
// ==/UserScript==

function syncSleep(milliseconds) {
    const date = Date.now();
    let currentDate = null;
    do {
      currentDate = Date.now();
    } while (currentDate - date < milliseconds);
  }


function addBtn() {
    console.log('addBtn');
    console.log($('div.next-table-inner > .next-table-body .next-table-row').length);

    $('div.next-table-inner > .next-table-body .next-table-row').each(function (index, element) {
        console.log(index, element);

        var tr = $(element);
        tr.find('a.pelelink').remove();

        var tmp = tr.find('span.basic-model-text:eq(3)');
        const skuId = $(tmp).text();

        var container = tmp.parent().parent();
        $(container).append(`
        <a href="https://sku.m.taobao.com/index?skuId=${skuId}&pha=true&disableNav=YES" target="_blank" style="color: currentcolor;" class="pelelink"><span class="basic-model-text" style="font-size: 16px; line-height: 25px;">打开前端</span></a>`);
    });
}

function getPrice(){
    console.log('getPrice');
    console.log($('div[class^="rax-view-v2 price--price--"]').text());
    console.log($('span[class^="price--mainPriceNum--"]').text());
    console.log($('span[class^="rax-text-v2 themeColor price--price--"]').text());
}

(function (open) {  
    XMLHttpRequest.prototype.open = function () {
      this.addEventListener('readystatechange', function () {

        console.log(this.readyState, this.responseURL);

        if (
          this.readyState == 4 &&
          this.responseURL.startsWith(
            'https://pm-seller.taobao.com/skuitem/searchSkuItem'
          )
        ) {
            console.log('start to parse the response!');
            var res = JSON.parse(this.responseText);
            if(res.code !== 0){
                console.log('error response!');
                return;
            }

            if(res.data === null && res.list === null && res.list.length === 0){
                console.log('no data!');
                return;
            }

            console.log('start to parse the data!');
            var data = res.data.list;
            for (var i = 0; i < data.length; i++) {
                var item = data[i];
                console.log(item.skuId, item.skuTitle);
            }

            setTimeout(function() {
                addBtn();
            }, 3000);
        }
      });
      open.apply(this, arguments);
    };
  })(XMLHttpRequest.prototype.open);

  (function() {
    'use strict';

    setTimeout(function() {
        getPrice();
    }, 3000);
  }
  )();
  