// ==UserScript==
// @name         管易-顺丰物流修正
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  方便同事们工作，减少失误
// @author       pelebl
// @match        https://v2.guanyierp.com/*
// @match        http://v2.guanyierp.com/*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=guanyierp.com
// @require      https://cdn.bootcdn.net/ajax/libs/jquery/3.5.1/jquery.js
// @updateURL    https://gitlab.com/older-open/userscripts/-/raw/main/gy-wuliu.user.js
// @downloadURL  https://gitlab.com/older-open/userscripts/-/raw/main/gy-wuliu.user.js
// ==/UserScript==
console.log('intercept');

function sleep(time) {
    return new Promise((resolve) => setTimeout(resolve, time));
}

// 批量标记物流参数
function batchMarkExpressParam(orders) {
    for (const order of orders) {
        console.log('找到了2');
        const tenantId = order.tenantId;
        const id = order.id;
        if (order.deliveryOrderStatusInfo.delivery != 2) {
            const weightTotal = parseFloat(order.weightTotal);
            const volumeTotal = parseFloat(order.volumeTotal);
            console.log(`${tenantId}-${id}-1 ${weightTotal}  ${volumeTotal}`);
    
            $.ajax({
                contentType: 'application/json',
                data: JSON.stringify({
                    total_weight: weightTotal,
                    total_volume: volumeTotal
                }),
                dataType: 'json',
                success: function (data) {
                  console.log(`${tenantId}-${id}-1  成功`);
                  console.log(data);
                },
                error: function () {
                  console.log(`${tenantId}-${id}-1  失败`);
                },
                processData: false,
                type: 'POST',
                url: `https://api.older.cn/api/v1/shunfeng/${tenantId}-${id}-1`,
            });
        } else {
            console.log(`${tenantId}-${id}-1 已发货`)
        }
    }
}

(function(){
    console.log(window.unsafeWindow)
    const originFetch = fetch;
    console.log(originFetch)
    window.unsafeWindow.fetch = (url, options) => {
        return originFetch(url, options).then(async response => {
            if (url == "/tc/delivery/delivery_order_header/data/print_list") {
                console.log('找到了');
                const responseClone = response.clone();
                let res = await responseClone.json();
                if (res.status == '200' && res.total > 0) {
                    batchMarkExpressParam(res.rows);
                }
            }
            return response;
        });
    }
})();


(function(open) {

    XMLHttpRequest.prototype.open = function() {
        this.addEventListener("readystatechange", function() {
            // console.log("readyState: "+this.readyState);
            // console.log(this.responseURL);
            if(this.readyState == 4 && this.responseURL.startsWith('https://v2.guanyierp.com/tc/delivery/delivery_order_header/data/print_list') ){
               console.log('找到了');
                sleep(3000).then(
                    ()=>{
                        const jsonResp = JSON.parse(this.responseText);
                        if(jsonResp.status == '200' && jsonResp.total > 0){
                            batchMarkExpressParam(jsonResp.rows);
                        }
                        
                    }
                );

            }
        });
        open.apply(this, arguments);
    };
})(XMLHttpRequest.prototype.open);
