// ==UserScript==
// @name         管易云使用
// @namespace    http://tampermonkey.net/
// @version      0.1.3
// @description  try to take over the world!
// @author       You
// @match        https://v2.guanyierp.com/index
// @match        http://v2.guanyierp.com/index
// @match        https://wuliu2.taobao.com/user/n/consign2.htm?*
// @match        https://wuliu2.taobao.com/user/consign2.htm?*
// @match        https://wuliu2.taobao.com/user/batch_consign.htm?*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=guanyierp.com
// @grant        GM_openInTab
// @grant        GM_setClipboard
// @require      https://cdn.bootcdn.net/ajax/libs/jquery/3.5.1/jquery.js
// @require      https://cdn.staticfile.org/jsbarcode/3.11.0/JsBarcode.all.min.js
// @require      https://cdn.staticfile.org/jQuery.print/1.2.0/jQuery.print.min.js
// @updateURL    https://gitlab.com/older-open/userscripts/-/raw/main/gy-jiangding.user.js
// @downloadURL  https://gitlab.com/older-open/userscripts/-/raw/main/gy-jiangding.user.js
// ==/UserScript==

// Read a page's GET URL variables and return them as an associative array.
function getUrlVars() {
  var vars = [],
    hash;
  var hashes = window.location.href
    .slice(window.location.href.indexOf('?') + 1)
    .split('&');
  for (var i = 0; i < hashes.length; i++) {
    hash = hashes[i].split('=');
    vars.push(hash[0]);
    vars[hash[0]] = hash[1];
  }
  return vars;
}

function sleep(time) {
  return new Promise((resolve) => setTimeout(resolve, time));
}

function syncSleep(milliseconds) {
  const date = Date.now();
  let currentDate = null;
  do {
    currentDate = Date.now();
  } while (currentDate - date < milliseconds);
}

(function () {
  'use strict';

  if (location.hostname === 'v2.guanyierp.com') {
    //一个是新元素，一个是body元素
    var mybutton, beasetag, printDiv, barcode;
    //创建新元素
    mybutton = document.createElement('div');
    //搜寻body元素
    beasetag = document.querySelector('body');
    //将新元素作为子节点插入到body元素的最后一个子节点之后
    beasetag.appendChild(mybutton);
    //可以通过mybutton.innerHTML = "<button type='button'>启动</button><br><button type='button'>关闭</button>"来写入其他元素，如多个按钮
    mybutton.innerHTML = '打印鉴别单[未打印中使用]';
    //css样式为
    //position:fixed;生成固定定位的元素，相对于浏览器窗口进行定位。元素的位置通过 "left", "top", "right" 以及 "bottom" 属性进行规定。
    //bottom:15px;距窗口底部15px
    //right:15px;距窗口右边15px
    //width:60px;内容的宽度60px
    //height:60px;内容的高度60px
    //background:black;内边距的颜色和内容的颜色设置为黑色，不包括外边距和边框
    //opacity:0.75;不透明度设置为0.75，1为完全不透明
    //color:white;指定文本的颜色为白色
    //text-align:center;指定元素文本的水平对齐方式为居中对齐
    //line-height:60px;设置行高，通过设置为等于该元素的内容高度的值，配合text-align:center;可以使div的文字居中
    //cursor:pointer;定义了鼠标指针放在一个元素边界范围内时所用的光标形状为一只手
    mybutton.style =
      'position:fixed;bottom:80%;right:30%;min-width:200px;width:auto;height:60px;background:black;opacity:0.75;color:white;text-align:center;line-height:60px;cursor:pointer;';
    mybutton.id = 'peleBtn';

    var newBtn = document.createElement('div');
    beasetag.appendChild(newBtn);
    newBtn.innerHTML = '去发货【未打印中使用】';
    newBtn.style =
      'position:fixed;bottom:60%;right:30%;min-width:200px;width:auto;height:60px;background:black;opacity:0.75;color:white;text-align:center;line-height:60px;cursor:pointer;';
    newBtn.id = 'wuliuBtn';

    printDiv = document.createElement('div');
    beasetag.appendChild(printDiv);
    printDiv.id = 'printDiv';

    // var style = $('<style>.class { background-color: blue; }</style>');
    var style = `<style>
        @media print {
        .pagebreak {
            clear: both;
            page-break-after:always;
            page-break-before:always
        }
    }
    </style>`;
    $('html > head').append(style);

    // barcode = document.createElement("img");
    // printDiv.appendChild(barcode);
    // barcode.id = "barcode";

    $(mybutton).on('click', function () {
      let platformCode = $('iframe')
        .eq(1)
        .contents()
        .find('#platformCode')
        .val();
      if (platformCode == undefined) {
        $('#peleBtn').text('先选择好订单吧');
        return;
      }

      $('#peleBtn').text('准备获取订单' + platformCode);
      $('#printDiv').empty();

      $.get(
        'https://api.older.cn/api/v1/appraisal/get/' + platformCode,
        function (data) {
          if (data.data != undefined && data.data.length > 0) {
            $('#peleBtn').text('准备打印' + platformCode);

            let printContent = '';
            for (let index = 0; index < data.data.length; index++) {
              const appraisal = data.data[index];
              let codes = appraisal.identification_number.split(',');
              let pageCount = (codes.length - 1) / 6 + 1;

              for (let page = 1; page <= pageCount; page++) {
                let pGoodsName = '<p>' + appraisal.platform_item_name;
                let pGoodsCount = '(' + appraisal.qty + '件)';
                let pShopName = '【杭州老酒集市】';
                // let pOrganName = '<p>机构名称：' + data.data.organ_name + '</p>';
                let pOrderNo =
                  '<p><img id="imgOrderNo' +
                  appraisal.platform_code +
                  '"/></p>';
                // let pIndenTitle = '<p>鉴别单号<p>';
                let pIndenContent = '';
                for (
                  let i = (page - 1) * 6;
                  i < codes.length && i < page * 6;
                  i++
                ) {
                  const code = codes[i];
                  pIndenContent +=
                    '<p>鉴别' + (i + 1) + '：<img id="code' + code + '"/></p>';
                }

                // let pageBreak = '<br class="pagebreak" />'

                printContent +=
                  '<div class="printPage">' +
                  pGoodsName +
                  pGoodsCount +
                  pShopName +
                  pOrderNo +
                  pIndenContent +
                  '</div>';
              }
              //printContent += '<div class="printPage">' + pOrderNo + pIndenContent + '</div>';
            }
            $('#printDiv').append(printContent);

            for (let index = 0; index < data.data.length; index++) {
              const appraisal = data.data[index];
              let codes = appraisal.identification_number.split(',');
              for (let i = 0; i < codes.length; i++) {
                JsBarcode('#code' + codes[i], codes[i], {
                  format: 'CODE128',
                  height: 30,
                  fontSize: 15,
                });
              }

              JsBarcode(
                '#imgOrderNo' + appraisal.platform_code,
                appraisal.platform_code,
                {
                  format: 'CODE128',
                  height: 30,
                  fontSize: 15,
                }
              );
            }

            $('#printDiv').print({
              title: '杭州老酒集市',
            });
            $('#peleBtn').text('打印完毕' + platformCode);
          } else {
            $('#peleBtn').text('未找到鉴定单' + platformCode);
            return;
          }
        }
      );
    });

    $(newBtn).on('click', function () {
      let platformCode = $('iframe')
        .eq(1)
        .contents()
        .find('#platformCode')
        .val();
      if (platformCode == undefined) {
        $('#wuliuBtn').text('先选择好订单吧');
        return;
      }

      //https://wuliu2.taobao.com/user/n/consign2.htm?orderId=528127644031&from=list&x-frames-allow-from=wuliu2#/
      //https://wuliu2.taobao.com/user/n/consign2.htm?tradeId=1652368586290139292&x-frames-allow-from=wuliu2#/
      let codes = platformCode.split(';');
      for (const code of codes) {
        let url =
          'https://wuliu2.taobao.com/user/consign2.htm?tradeId=' +
          code +
          '&x-frames-allow-from=wuliu2';
        GM_openInTab(url, { active: true, setParent: true });
      }
    });
  } else if (location.hostname == 'wuliu2.taobao.com') {
    if (location.pathname == '/user/batch_consign.htm') {
      window.open('about:blank', '_self').close();
      return;
    }

    let orderNo = getUrlVars()['tradeId'];
    console.log(orderNo);
    GM_setClipboard('SJZX' + orderNo, 'text');

    sleep(1000).then(function () {
      console.log('hehe');
      let inputTxt = $(
        '#icestark-container > div > div > div.consign-page-container > div > div > div:nth-child(4) > div.next-card.next-card-free.next-card-show-divider.choose-delivery-mode > div.next-card-content-container > div > div > div.flex.flex-align-top > div.cp-code-selector > span > span.next-input.next-medium.next-select-inner > span.next-select-values.next-input-text-field'
      ).eq(0);
      console.log(inputTxt);
      inputTxt.click();
      // sleep(2000).then(function(){
      //     console.log(typeof chrome.experimental.clipboard.executePaste);
      //      let tt = $('#icestark-container > div > div > div.consign-page-container > div > div > div:nth-child(4) > div.next-card.next-card-free.next-card-show-divider.choose-delivery-mode > div.next-card-content-container > div > div > div.flex.flex-align-top > div.cp-code-selector > span > span.next-input.next-focus.next-medium.next-select-inner > span.next-select-values.next-input-text-field > em > div > span:nth-child(1)');
      //      tt.val('abc');
      // });

      // console.log($('iframe').eq(0).contents());
      // let inputTxt = $('iframe').eq(0).contents().find('#icestark-container > div > div > div.consign-page-container > div > div > div:nth-child(4) > div.next-card.next-card-free.next-card-show-divider.choose-delivery-mode > div.next-card-content-container > div > div > div.flex.flex-align-top > div.cp-code-selector > span');
      // console.log(inputTxt.eq(0));
      // inputTxt.eq(0).click();

      // sleep(2000).then(function(){
      //     let ff = $('iframe').eq(0).contents();
      // let tt = ff.find('#icestark-container > div > div > div.consign-page-container > div > div > div:nth-child(4) > div.next-card.next-card-free.next-card-show-divider.choose-delivery-mode > div.next-card-content-container > div > div > div.flex.flex-align-top > div.cp-code-selector > span > span.next-input.next-error.next-medium.next-select-inner > span.next-select-values.next-input-text-field > span > input').eq(0);
      // console.log(tt);
      // tt.value = "abc";

      // });
    });
  }
})();
