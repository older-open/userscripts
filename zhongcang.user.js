// ==UserScript==
// @name         中藏鉴定工具
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  方便同事们工作，减少失误
// @author       pelebl
// @match        https://paimai.taobao.com/console/seller/index.htm
// @match        https://paimai.taobao.com/console/org/index.htm
// @match        https://z.mj8.com.cn/manage/goods_order/add.html
// @match        https://cn.zcxart.com/manage/goods_order/add.html
// @icon         https://www.google.com/s2/favicons?sz=64&domain=taobao.com
// @require      https://cdn.bootcdn.net/ajax/libs/jquery/3.5.1/jquery.js
// @grant        GM_setValue
// @grant        GM_getValue
// ==/UserScript==
console.log('intercept');

function sleep(time) {
    return new Promise((resolve) => setTimeout(resolve, time));
}


(function() {
    'use strict';

    // Your code here...
    if (location.hostname === 'cn.zcxart.com' || location.hostname === 'z.mj8.com.cn'){

        $('.layui-form').before('<div class="layui-form-mid layui-word-aux">\n' +
            '\t\t\t\t\t<a class="layui-btn layui-btn-normal layui-btn-xs" id="autoCopy">请先选取订单</a>\n' +
            '\t\t\t\t</div>');

        $('#autoCopy').click(function () {
            const orderId = GM_getValue('order-id', '');
            const ordersRsp = GM_getValue('ordersRsp', '');
            if(orderId === '' || ordersRsp === ''){
                console.log('failed');
            }else{
                const orders = JSON.parse(ordersRsp).data.list;
                let targetOrder = null;
                for (const order of orders) {
                    if(order.bizOrderId === orderId){
                        targetOrder = order;
                    }
                }
                console.log(targetOrder);

                if(targetOrder !== null){
                    $("input[name='order_name']").val(targetOrder.item.title);
                    //$("input[name='goods_num']").val(targetOrder.item.quantity);
                    if(targetOrder.sellerDeliveryLogistics != undefined && targetOrder.sellerDeliveryLogistics.logisticsNum != undefined){
                        $("input[name='express_code']").val(targetOrder.sellerDeliveryLogistics.logisticsNum);
                    }
                    $("input[name='ali_order_sn']").val(targetOrder.bizOrderId);
                    $("input[name='ali_code']").val(targetOrder.identificationCases[0].id);
                }
            }
        });

        setInterval(()=>{
            const orderId = GM_getValue('order-id', '');
            const autoCpTxt = $('#autoCopy').text();
            if(orderId !== ''){
                let txt = '自动填充'+orderId;
                if(autoCpTxt !== txt){
                    $('#autoCopy').text(txt);
                }
            }else if(autoCpTxt !== '请先选取订单'){
                $('#autoCopy').text('请先选取订单');
            }
        }, 1000)

    }
})();


// var jsonData = null;

(function(open) {

    XMLHttpRequest.prototype.open = function() {
        this.addEventListener("readystatechange", function() {
            console.log("readyState: "+this.readyState);
            if(this.readyState == 4 && (this.responseURL.startsWith('https://paimai.taobao.com/console/seller/json/getApplyListForSeller.do') || this.responseURL.startsWith('https://paimai.taobao.com/console/org/json/getApplyListForInstitution.do'))){
                GM_setValue('ordersRsp', this.responseText);
                GM_setValue('order-id', '');
                // jsonData = JSON.parse (this.responseText).data.list;
                sleep(3000).then(
                    ()=>{
                        $('tbody:first>.next-table-row').each(
                            function(){
                                const orderId = $(this).find('.next-table-cell:eq(1) span.basic-model-text').text().substr(0,19);
                                console.log(orderId);
                                $(this).find('.next-table-cell.last>.next-table-cell-wrapper>div>div>div')
                                    .append('<div class="next-box" style="flex-flow: row nowrap; margin: 3px;"><button type="button" class="next-btn next-small next-btn-normal copy-order"">'+orderId+'</button></div>')
                            }
                        );

                        $('.copy-order').click(function(){
                            //alert($(this).text());
                            GM_setValue('order-id', $(this).text());
                            console.log(GM_getValue('order-id', true));
                        });
                        //$('tbody:first>.next-table-row>.next-table-cell.last>.next-table-cell-wrapper>div>div>div').append('<div class="next-box" style="flex-flow: row nowrap; margin: 3px;"><button type="button" class="next-btn next-small next-btn-normal"><a href="https://wuliu.taobao.com/user/consign.htm?trade_id=2794840599070650865" target="_blank">查看123</a></button></div>')

                    }
                );

            }
        });
        open.apply(this, arguments);
    };
})(XMLHttpRequest.prototype.open);