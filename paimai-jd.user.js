// ==UserScript==
// @name         拍卖鉴别单处理
// @namespace    http://tampermonkey.net/
// @version      0.2.4
// @description  处理鉴别单，处理二段物流
// @author       pele
// @match        https://paimai.taobao.com/console/seller/index.htm
// @icon         https://www.google.com/s2/favicons?sz=64&domain=taobao.com
// @require      https://cdn.bootcdn.net/ajax/libs/jquery/3.5.1/jquery.js
// @require      https://cdn.jsdelivr.net/npm/js-md5@0.7.3/src/md5.min.js
// @grant        none
// @updateURL    https://gitlab.com/older-open/userscripts/-/raw/main/paimai-jd.user.js
// @downloadURL  https://gitlab.com/older-open/userscripts/-/raw/main/paimai-jd.user.js
// ==/UserScript==

function createRequest(method, url, data, headers) {
  return new Promise((resolve, reject) => {
      var xhr = new XMLHttpRequest();
      xhr.open(method, url);
      xhr.onload = function() {
          if (this.status >= 200 && this.status < 300) {
              resolve(xhr.response);
          } else {
              reject({
                  status: this.status,
                  statusText: xhr.statusText
              });
          }
      };
      xhr.onerror = function() {
          reject({
              status: this.status,
              statusText: xhr.statusText
          });
      };
      if (headers) {
          Object.keys(headers).forEach(key => {
              xhr.setRequestHeader(key, headers[key]);
          });
      }
      xhr.send(JSON.stringify(data));
  });
}

function generateSign(appSecret, params) {
  let sortedParams = Object.keys(params).sort().reduce((result, key) => {
      result[key] = params[key];
      return result;
  }, {});

  let paramString = appSecret + Object.values(sortedParams).join('') + appSecret;
  let md5Hash = md5(paramString).toUpperCase();
  return md5Hash;
}

function post(appKey, appSecret, apiUrl, method, params) {
  let sign = generateSign(appSecret, params);
  let data = {
      // appkey: appKey,
      // method: method,
      sign: sign,
      ...params
  };

  let headers ={
    'Content-Type': 'application/json;charset=UTF-8',
    'Accept': 'application/json'
  };

  return createRequest('POST', apiUrl, data, headers);
}

let appKey = '179508';
let appSecret = '81f4bf36886c4196b709ae0ffe3e6759';
let apiUrl = '	https://v2.api.guanyierp.com/rest/erp_open';
let method = 'gy.erp.items.get';
let sessionkey = 'f5043ebac76b4e7ca6e8636007a3384c';
let params = {method: method, appkey:appKey, sessionkey: sessionkey, code:'1234', page_no:1, page_size:10};

post(appKey, appSecret, apiUrl, method, params)
    .then(data => console.log(data))
    .catch(error => console.error(error));

let stopRun = false;
let doNext = false;

function sleep(time) {
    return new Promise((resolve) => setTimeout(resolve, time));
  }

function syncSleep(milliseconds) {
  const date = Date.now();
  let currentDate = null;
  do {
    currentDate = Date.now();
  } while (currentDate - date < milliseconds);
}

function nextPage() {
  if ($('button.next-next').attr('disabled') == undefined) {
    $('button.next-next').click();
  } else {
    if (
      $('.next-pagination-list > button.next-pagination-item:first').length >
        0 &&
      $('.next-pagination-list > button.next-pagination-item:first').is(
        '.next-current'
      ) == false
    ) {
      $('.next-pagination-list > button.next-pagination-item:first').click();
    }
  }

  sleep(3000).then(function () {
    if(doNext && !stopRun){
        nextPage();
    }
  });
}

(function () {
  'use strict';

  // Your code here...
  console.log('coming!');
  //一个是新元素，一个是body元素
  var mybutton, beasetag, printDiv, barcode;
  //创建新元素
  mybutton = document.createElement('div');
  //搜寻body元素
  beasetag = document.querySelector('body');
  //将新元素作为子节点插入到body元素的最后一个子节点之后
  beasetag.appendChild(mybutton);
  //可以通过mybutton.innerHTML = "<button type='button'>启动</button><br><button type='button'>关闭</button>"来写入其他元素，如多个按钮
  mybutton.innerHTML = '点击开始扫描';
  //css样式为
  //position:fixed;生成固定定位的元素，相对于浏览器窗口进行定位。元素的位置通过 "left", "top", "right" 以及 "bottom" 属性进行规定。
  //bottom:15px;距窗口底部15px
  //right:15px;距窗口右边15px
  //width:60px;内容的宽度60px
  //height:60px;内容的高度60px
  //background:black;内边距的颜色和内容的颜色设置为黑色，不包括外边距和边框
  //opacity:0.75;不透明度设置为0.75，1为完全不透明
  //color:white;指定文本的颜色为白色
  //text-align:center;指定元素文本的水平对齐方式为居中对齐
  //line-height:60px;设置行高，通过设置为等于该元素的内容高度的值，配合text-align:center;可以使div的文字居中
  //cursor:pointer;定义了鼠标指针放在一个元素边界范围内时所用的光标形状为一只手
  mybutton.style =
    'position:fixed;bottom:20%;right:30%;min-width:300px;width:auto;height:60px;background:black;opacity:0.75;color:white;text-align:center;line-height:60px;cursor:pointer;';
  mybutton.id = 'peleBtn';

  $('#peleBtn').click(function () {
    if ($('#peleBtn').text() == '点击开始扫描') {
      stopRun = false;
      nextPage();
    } else {
      stopRun = true;
      $('#peleBtn').text('点击开始扫描');
    }
  });
})();

(function (open) {
  XMLHttpRequest.prototype.open = function () {
    this.addEventListener('readystatechange', function () {
      
      if (
        this.readyState == 4 &&
        this.responseURL.startsWith(
          'https://paimai.taobao.com/console/seller/json/getApplyListForSeller.do'
        )
      ) {
        doNext = false;
        console.log(this.responseURL);
        // console.log(this.responseText);

        let resp = JSON.parse(this.responseText);
        if (
          resp.code == 0 &&
          resp.data != undefined &&
          resp.data.list != undefined
        ) {
          let orderList = resp.data.list;
          for (const order of orderList) {
            // 1 待机构收货  2 待完成鉴别  3 待机构发货  4 已发给买家  5 待机构退货  6 已退回商家
            let applyStatus = order.applyStatus;

            let platformCode = order.mainOrderId;
            let platformItemName = order.item.title;
            let qty = order.item.quantity;
            let organName = order.institution.institutionName;

            let identificationNumber = order.identificationCases[0].id;
            for (
              let index = 1;
              index < order.identificationCases.length;
              index++
            ) {
              const item = order.identificationCases[index];
              identificationNumber += ',' + item.id;
            }

            let logisticsCompany = order.logistics.logisticsCompany;
            let logisticsNum = order.logistics.logisticsNum;
            let logisticsNum2 = '';
            if(order.sellerDeliveryLogistics != undefined){
              logisticsNum2 = order.sellerDeliveryLogistics.logisticsNum;
            }
            let remark = order.remark === undefined ? '' : order.remark;

            let unixTimestamp = new Date(order.serviceCreateTime);
            let serviceTime = unixTimestamp.toLocaleString();
            let serviceMode = order.serviceMode;

            $('#peleBtn').text(`[${serviceTime}] ${platformCode}`);

            if ((applyStatus === 1 && serviceMode == 1) || (applyStatus === 3 && serviceMode == 2) || (applyStatus === 7 && serviceMode == 1) ) {
              $.ajax({
                contentType: 'application/json',
                data: JSON.stringify({
                  platform_code: platformCode,
                  platform_item_name: platformItemName,
                  qty: qty,
                  organ_name: organName,
                  identification_number: identificationNumber,
                  service_mode: serviceMode
                }),
                dataType: 'json',
                success: function (data) {
                  console.log(platformCode + ' 鉴定单上传 done');
                },
                error: function () {
                  console.log('failed');
                },
                processData: false,
                type: 'POST',
                url: 'https://apigate.older.cn/api/updateTag',
              });
            } else if (applyStatus === 4 && logisticsNum != logisticsNum2) {
              $.ajax({
                contentType: 'application/json',
                data: JSON.stringify({
                  platform_code: platformCode,
                  express_company: logisticsCompany,
                  express_number: logisticsNum,
                  note: remark,
                }),
                dataType: 'json',
                success: function (data) {
                  console.log( platformCode + ' 物流上传 done');
                  console.log(data);
                },
                error: function (err) {
                  console.log('failed');
                },
                processData: false,
                type: 'POST',
                url: 'https://api.older.cn/api/v1/appraisal/put_express',
              });
            }

            doNext = true;
          }
        }
      }
    });
    open.apply(this, arguments);
  };
})(XMLHttpRequest.prototype.open);
